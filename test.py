def time_to_seconds(time):
    """ converts a hh:mm:ss time to total seconds and return it as an int """
    hours_to_sec=60*(60*get_hrs(time))
    return hours_to_sec

def get_hrs(time):
    """ extract and return the numbers of seconds from a time string """
    time_list = time.split(':')
    hours = int(time_list[0])
    return hours

time=input("input:")
print(time_to_seconds(time))